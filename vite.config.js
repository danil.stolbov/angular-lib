import { defineConfig } from 'vite';
import { resolve } from 'path';
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js';

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'lib/angular/index.js'),
      name: 'angular-lib',
      fileName: 'angular-lib',
      formats: ['es', 'cjs']
    },
    outDir: resolve(__dirname, 'dist'),
    sourcemap: true,
    emptyOutDir: true
  },
  plugins: [
    cssInjectedByJsPlugin()
  ]
});
