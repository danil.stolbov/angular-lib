export default function ngModel() {
  return {
    link: (scope, element, attr) => {
      const variable = element.getAttribute('ng-model');

      const value = attr.type === 'checkbox' ? 'checked' : 'value';

      if (!scope[variable]) {
        scope.eval(`${variable} = '${element[value]}'`);
      }

      scope.$watch(variable, () => {
        if (attr.type === 'file') {
          return;
        }

        element[value] = scope.eval(variable);
      });

      element.addEventListener('input', () => {
        scope.eval(`${variable} = '${element[value]}'`);
        scope.$apply();
      });
    }
  };
}
