export default function ngHide() {
  return {
    link: (scope, element, attr) => {
      const variable = element.getAttribute('ng-hide');

      const toggleVisibility = () => {
        if (scope[variable]) {
          element.classList.add('ng-hide');
          return;
        }

        element.classList.remove('ng-hide');
      };

      toggleVisibility();
      scope.$watch(variable, toggleVisibility);
    }
  };
}
