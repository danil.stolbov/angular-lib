export default function ngFile() {
  return {
    link: (scope, element) => {
      const [ctrl, variable] = element.getAttribute('ng-file').split('.');

      element.addEventListener('change', ({ target: { files: [file] } }) => {
        scope[ctrl][variable] = file;
        scope.$apply();
      });
    }
  };
}
