export default function ngBold() {
  return {
    link: (scope, element) => {
      const variable = element.getAttribute('ng-bold');

      const fontWeight = variable ? 'bold' : 'normal';

      const addStyleFontWeight = () => {
        element.style.fontWeight = fontWeight;
      };

      addStyleFontWeight();
      scope.$watch(variable, addStyleFontWeight);
    }
  };
}
