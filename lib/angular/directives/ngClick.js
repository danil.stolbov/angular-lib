export default function ngClick() {
  return {
    link: (scope, element) => {
      const variable = element.getAttribute('ng-click');

      element.addEventListener('click', () => {
        scope.eval(variable);
        scope.$apply();
      });
    }
  };
}
