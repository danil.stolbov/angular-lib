export default function ngIf() {
  return {
    link: (scope, element) => {
      const variable = element.getAttribute('ng-if');

      const toggleVisibility = () => {
        element.hidden = !scope.eval(variable);
      };

      toggleVisibility();
      scope.$watch(variable, toggleVisibility);
    }
  };
}
