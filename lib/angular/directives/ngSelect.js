export default function ngSelect() {
  return {
    link: (scope, element) => {
      const value = element.getAttribute('ng-select');

      const textSelection = () => {
        const range = new Range();
        const index = element.firstChild.textContent.indexOf(scope.eval(value));
        const select = document.createElement('span');

        if (index < 0) {
          return;
        }

        range.setStart(element.firstChild, index);
        range.setEnd(element.firstChild, index + scope.eval(value).length);
        select.classList.add('ng-select');
        range.surroundContents(select);
      };

      textSelection();
      scope.$watch(value, textSelection);
    }
  };
}
