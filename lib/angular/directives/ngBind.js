export default function ngBind() {
  return {
    link: (scope, element) => {
      const variable = element.getAttribute('ng-bind');

      const bindValues = () => {
        element.textContent = scope.eval(variable);
      };

      bindValues();
      scope.$watch(variable, bindValues);
    }
  };
}
