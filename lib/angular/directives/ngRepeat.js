export default function ngRepeat() {
  return {
    link: (scope, element) => {
      const variable = element.getAttribute('ng-repeat');
      const [name, value] = variable.split(' in ');
      const { parentNode } = element;
      element.removeAttribute('ng-repeat');
      const currentElement = element.cloneNode(true);

      currentElement.removeAttribute('ng-repeat');
      currentElement.classList.add('ng-delete');
      element.remove();

      const renderElements = () => {
        const fragment = document.createDocumentFragment();

        parentNode.querySelectorAll('.ng-delete').forEach(node => node.remove());
        scope.eval(value).forEach(item => {
          scope[name] = item;

          const clone = currentElement.cloneNode(true);
          clone.innerHTML = clone.innerHTML.replace(/{{(.*?)}}/g, (_, p1) => scope.$$parse(scope, p1));
          fragment.append(clone);
          scope.$$compile(clone);
          clone.querySelectorAll('*').forEach(scope.$$compile);
        });

        parentNode.append(fragment);
      };

      renderElements();
      scope.$watch(value, renderElements);
    }
  };
}
