export default function ngSrc() {
  return {
    link: (scope, element) => {
      const value = element.getAttribute('ng-src');

      const setUrl = () => {
        const evalValue = scope.eval(value);

        if (!evalValue) {
          return;
        }

        element.src = evalValue;
      };

      setUrl();
      scope.$watch(value, setUrl);
    }
  };
}
