export default function ngShow() {
  return {
    link: (scope, element) => {
      const variable = element.getAttribute('ng-show');

      const toggleVisibility = () => {
        if (scope[variable]) {
          element.classList.remove('ng-hide');
          return;
        }

        element.classList.add('ng-hide');
      };

      toggleVisibility();
      scope.$watch(variable, toggleVisibility);
    }
  };
}
