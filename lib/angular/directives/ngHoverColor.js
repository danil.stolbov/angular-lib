export default function ngHoverColor() {
  return {
    link: (scope, element) => {
      const variable = element.getAttribute('ng-hover-color');
      const defaultColor = element.style.color;

      element.onmouseover = () => {
        element.style.color = variable;
      };

      element.onmouseout = () => {
        element.style.color = defaultColor;
      };
    }
  };
}
