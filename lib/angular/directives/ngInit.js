export default function ngInit() {
  return {
    link: (scope, element) => {
      scope.eval(element.getAttribute('ng-init'));
      scope.$apply();
    }
  };
}
