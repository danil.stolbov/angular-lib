export default function $setTimeout($rootScope) {
  return function $timeout(callback, delay) {
    setTimeout(() => {
      callback();
      $rootScope.$applyAsync();
    }, delay);
  };
}

$setTimeout.dependencies = ['$rootScope'];
