export default function $http($rootScope) {
  const METHODS_HTTP = {
    GET_METHOD: 'GET',
    POST_METHOD: 'POST',
    DELETE_METHOD: 'DELETE'
  };

  const responseTypeHandlers = {
    'json': response => response.json(),
    'text': response => response.text(),
    'arraybuffer': response => response.arrayBuffer(),
    'blob': response => response.blob()
  };

  class HttpRequest {
    constructor(baseUrl) {
      this.baseUrl = baseUrl;
    }

    get(url, config) {
      return this.request(Object.assign({}, config, { method: METHODS_HTTP.GET_METHOD, url }));
    }

    post(url, data, config) {
      return this.request(Object.assign({}, config, { method: METHODS_HTTP.POST_METHOD, url, data }));
    }

    delete(url, config) {
      return this.request(Object.assign({}, config, { method: METHODS_HTTP.DELETE_METHOD, url }));
    }

    request(config) {
      const {
        method = METHODS_HTTP.GET_METHOD,
        url,
        headers,
        data,
        responseType = 'text'
      } = config;

      const baseUrl = new URL(url, this.baseUrl);

      return fetch(baseUrl.href, {
        method,
        headers,
        body: data
      })
        .then(res => {
          $rootScope.$applyAsync();

          if (!res.ok) {
            throw new Error(`${method} ${res.url} ${res.statusText}`);
          }

          return responseTypeHandlers[responseType](res);
        });
    }
  }

  return new HttpRequest();
}

$http.dependencies = ['$rootScope'];
