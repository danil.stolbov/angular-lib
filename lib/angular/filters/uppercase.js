export default function uppercase(value) {
  return value.toUpperCase();
}
