function extractFunctionArguments(func) {
  const args = func.toString().match(/\((.*)\)/)[1];

  return args.split(',').map(arg => arg.trim());
}

export default function assignDependencies(args) {
  let fn = args;

  if (Array.isArray(args)) {
    fn = args.pop();
    fn.dependencies = args;
  } else if ((/inject/).test(fn)) {
    fn.dependencies = extractFunctionArguments(fn);
  }

  if (!fn.dependencies) {
    fn.dependencies = [];
  }

  return fn;
}
