const changeAttributeName = str => str.replace(/[-_:' ']/gi, ' ').split(' ')
  .reduce((prev, curr) => prev + curr.charAt(0).toUpperCase() + curr.slice(1));

export default changeAttributeName;
