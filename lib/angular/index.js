import * as defaultDirectives from './directives/index.js';
import * as defaultServices from './services/index.js';
import * as defaultFilters from './filters/index.js';
import * as utils from './utils/index.js';
import './style.css';

function SmallAngular() {
  const rootScope = window;
  const directives = {};
  const watchers = {};
  const controllers = {};
  const components = {};
  const services = {};
  const configs = [];
  const filters = {};

  function directive(name, fn) {
    directives[name] = fn;

    return this;
  }

  function getDependencies(dependencies) {
    return dependencies.map(dep => {
      if (dep === '$rootScope' || dep === '$scope') {
        return rootScope;
      }

      if (!Reflect.has(services, dep)) {
        throw new Error(`Service ${dep} not found`);
      }

      return services[dep];
    });
  }

  function controller(name, fn) {
    controllers[name] = utils.setDependencies(fn);

    return this;
  }

  function constant(name, value) {
    services[name] = value;

    return this;
  }

  function component(name, fn) {
    components[name] = fn;

    return this;
  }

  function config(fn) {
    configs.push(utils.setDependencies(fn));

    return this;
  }

  function initBaseConfigs() {
    configs.forEach(fn => {
      const deps = getDependencies(fn.dependencies);
      fn(...deps);
    });
  }

  function service(name, fn) {
    const funcWithDeps = utils.setDependencies(fn);
    const funcArgs = getDependencies(funcWithDeps.dependencies);

    services[name] = funcWithDeps(...funcArgs);

    return this;
  }

  function parse(scope, str) {
    const [variable, ...filterArr] = str.split('|').map(str => str.trim());
    const result = scope.eval(variable);

    return filterArr.reduce((prev, curr) => filters[curr](prev), result);
  }

  const getAllAttributes = ({ attributes }) => {
    const allAttributes = { angularAttributes: {}, notAngularAttributes: {} };

    for (const attr of attributes) {
      const { name, value } = attr;
      const changedName = utils.changeAttributeName(name);
      const key = directives[changedName] ? 'angularAttributes' : 'notAngularAttributes';

      allAttributes[key][changedName] = value;
    }

    return allAttributes;
  };

  const compile = node => {
    const { notAngularAttributes, angularAttributes } = getAllAttributes(node);
    const tagName = node.nodeName.toLowerCase();
    const component = components[tagName];

    if (component) {
      const { template, controller, link, controllerAs } = component();

      if (template) {
        node.innerHTML = template;
      }

      const Control = controllers[controller];

      if (!Control && link) {
        link(rootScope, node, notAngularAttributes);
      }

      if (!Control) {
        throw new Error('Controller not found');
      }

      const controlDependencies = getDependencies(Control.dependencies);
      const controlInst = new Control(...controlDependencies);

      if (controllerAs) {
        rootScope[controllerAs] = controlInst;
      }

      node.querySelectorAll('*').forEach(compile);

      return;
    }

    for (const name of Object.keys(angularAttributes)) {
      directives[name]().link(rootScope, node, notAngularAttributes);
    }
  };

  rootScope.$apply = () => {
    for (const fn of Object.values(watchers)) {
      fn();
    }
  };

  this.module = appName => {
    this.appName = appName;
    return this;
  };

  rootScope.$watch = (param, cb) => {
    if (!(param in watchers)) {
      watchers[param] = [];
    }

    watchers[param] = cb;
  };

  this.bootstrap = (node = document.querySelector('[ng-app]')) => {
    if (!node) {
      return;
    }

    compile(node);
    node.querySelectorAll('*').forEach(compile);
  };

  rootScope.$applyAsync = () => setTimeout(rootScope.$apply, 50);

  rootScope.$$compile = compile;
  rootScope.$$parse = parse;

  for (const key in defaultServices) {
    service(key, defaultServices[key]);
  }

  this.directive = directive;
  this.constant = constant;
  this.config = config;
  this.controller = controller;
  this.component = component;
  this.service = service;

  Object.assign(directives, defaultDirectives);
  Object.assign(filters, defaultFilters);
  setTimeout(() => {
    initBaseConfigs();
    this.bootstrap();
  }, 200);
}

const angular = new SmallAngular();

window.angular = angular;
export default angular;
